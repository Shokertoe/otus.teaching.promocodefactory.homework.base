﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(Guid id);
       Task<List<T>> GetByIdListAsync(List<Guid> ids);
        Task CreateAsync(T model);
        Task UpdateAsync(T model);
        Task DeleteByIdAsync(Guid id);
    }
}