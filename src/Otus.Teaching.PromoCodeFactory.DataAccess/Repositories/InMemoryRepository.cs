﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T model)
        {
            Data = Data.Append(model);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T model)
        {
            var oldVal = Data.Single(item => item.Id == model.Id);
            oldVal = model;
            return Task.CompletedTask;
        }

        public Task DeleteByIdAsync(Guid id)
        {
            Data = Data.Except(Data.Where(item => item.Id == id));
            return Task.CompletedTask;
        }

        public Task<List<T>> GetByIdListAsync(List<Guid> ids)
        {
            var result = new List<T>();
            ids.ForEach(id =>
                    result.Add(Data.FirstOrDefault(item => item.Id == id))
                );

            return Task.FromResult(result.Where(x => x != null).ToList());
        }
    }
}