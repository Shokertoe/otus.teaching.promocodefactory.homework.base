﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public partial class EmployeesController
    {
        public record EmployeeInput()
        {
            public string FirstName { get; init; }
            public string LastName { get; init; }
            public string Email { get; init; }
            public List<Guid> Roles { get; init; }
        }
    }
}